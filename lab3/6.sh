#! /bin/zsh

file="$PWD/5.out"
lines_total="$(wc -l --total=only $file)"
idx=1 # number of the current line
cnt=1 # amount of processes with same ppid
avg_sleep=0

while [[ $idx -le $lines_total ]]; do
    if [[ $idx -eq $lines_total ]]; then
        echo "\n" >> $file
    fi
    line="$(awk "NR==${idx}" $file)"
    ppid="$(echo $line | awk '{ split($3, l, "="); print l[2] }')"
    time="$(echo $line | awk '{ split($5, l, "="); print l[2] }')"
    next_line="$(awk "NR==$(( $idx + 1 ))" $file)"
    next_ppid="$(echo $next_line | awk '{ split($3, l, "="); print l[2] }')"

    if [[ "${ppid}" == "${next_ppid}" ]]; then
        avg_sleep="$(( $avg_sleep + $time ))"
        let cnt++
        let idx++
    else
        if [[ $cnt -eq 1 ]]; then
            avg_sleep="${time}"
        else
            avg_sleep="$(( $avg_sleep + $time ))"
            avg_sleep="$(echo "scale=3; $avg_sleep / $cnt" | bc)"
        fi

        result="Average_Sleeping_Children_of_ParentID=${ppid} is ${avg_sleep}"
        sed -i "$(( $idx + 1 ))i${result}" $file

        avg_sleep=0
        cnt=1
        idx="$(( $idx + 2 ))"
        let lines_total++
    fi
done
