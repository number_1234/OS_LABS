#! /bin/bash

OUT_FILE="$PWD/1.out"

echo "$(pgrep -u $USER | wc -l) total"
for pid in $(pgrep -u $USER); do
    command="$(cat /proc/${pid}/cmdline | tr -d '\0')"
    echo "${pid}:${command}" >> $OUT_FILE
done
