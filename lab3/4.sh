#! /bin/bash

OUT_FILE="$PWD/4.out"
for pid in $(ps -eo pid=); do
    diff="$(cat /proc/${pid}/statm 2>&1 | awk '{print (( $2 - $3 ))}')"
    echo "${diff} ${pid}"
done | sort -rn | awk '{print $2":"$1}' >> $OUT_FILE