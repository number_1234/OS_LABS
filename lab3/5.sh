#! /bin/bash

for pid in $(ps -eo pid=); do
    if [[ -d "/proc/${pid}" ]]; then
        ppid="$(grep 'PPid' /proc/${pid}/status | awk '{ print $2 }')"
        sum_exec_runtime="$(grep 'sum_exec_runtime' /proc/${pid}/sched | awk '{ print $3 }')"
        nr_switches="$(grep 'nr_switches' /proc/${pid}/sched | awk '{ print $3 }')"
        avg_atom="$(echo "scale = 3; ${sum_exec_runtime} / ${nr_switches}" | bc)"
        echo "${ppid} ${pid} ${avg_atom}"
    fi
done \
  | sort -n \
  | awk '{ print "ProcessID="$2 " : " "Parent_ProcessID="$1 " : " "Average_Time="$3 }' >> 5.out