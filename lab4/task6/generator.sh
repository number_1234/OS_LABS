#! /bin/bash

while true; do
    read data
    case $data in
    "TERM")
        kill -15 $(cat $PWD/.pid) # SIGTERM
        exit 0
        ;;
    *)
        echo "read string: ${data}"
        ;;
    esac
done