#! /bin/bash

echo "Program $0 started!"

quit() {
    echo "Program terminated by SIGTERM, exiting..."
    exit 0
}

echo "$$" > .pid
trap 'quit' 15
while true; do :; done # do nothing