#! /bin/bash


usr1() {
    result="$(( $result + 2 ))"
}

usr2() {
    result="$(( $result * 2 ))"
}

quit() {
    echo "Program terminated by SIGTERM, exiting..."
    exit 0
}

echo "$$" > .pid

trap 'usr1' 10
trap 'usr2' 12
trap 'quit' 15

result=1
prev_result=$result
while true; do
    if [[ $result -ne $prev_result ]]; then
        echo "result: ${result}"
        prev_result=$result
    fi
    sleep 1
done