#! /bin/bash

read handler_pid <.pid

while true; do
    read input
    case $input in
    "+")
        kill -10 $handler_pid # USR1
        ;;
    "*")
        kill -12 $handler_pid # USR2
        ;;
    "TERM")
        kill -15 $handler_pid
        exit 0
        ;;
    *)
        :
        ;;
    esac
    sleep 1
done