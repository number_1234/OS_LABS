#! /bin/bash

quit() {
    echo $2
    kill $(cat $PWD/.pid)
    exit $1
}

result=1
mode="ADD"
(tail -n 0 -f $PWD/file.txt) |
while true; do
    read query
    if [ "${query}" -eq "${query}" ] 2>/dev/null; then
        case $mode in
            "ADD")
                result="$(( $result + $query ))"
                ;;
            "MULTIPLY")
                result="$(( $result * $query ))"
                ;;
        esac
        echo "result = ${result}"
    elif [[ ${#query} -eq 1 ]]; then
        case $query in
            "+")
                mode="ADD"
                ;;
            "*")
                mode="MULTIPLY"
                ;;
            *)
                quit 1 "Error! Exiting..."
                ;;
        esac
        continue
    elif [[ "${query}" == "QUIT" ]]; then
        quit 0 "Exiting..."
        echo "result = ${result}"
    else
        quit 1 "Invalid data entered! Exiting..."
    fi
done
