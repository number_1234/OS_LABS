#! /bin/bash

date_started="$(date '+%d_%m_%Y-%H_%M')"
mkdir $PWD/test 2>/dev/null && {
    echo "catalog 'test' was created successfully" > $PWD/report
    touch $PWD/test/${date_started}.file
}

ping www.net_nikogo.ru 2>>$PWD/report
