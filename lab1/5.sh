#! /bin/bash

printf "
--------------
(1) vim\n
--------------
(2) htop\n
--------------
(3) print 'Hello!'\n
--------------
(4) exit\n
--------------
"

INPUT=0
read -p "Enter the value 1..4 : " INPUT
case $INPUT in
    1)
        vim
        ;;
    2)
        htop
        ;;
    3)
        echo "Hello!"
        ;;
    4)
        exit 0
        ;;
    *)
        echo "Wrong value!"
        exit 1
        ;;
esac
