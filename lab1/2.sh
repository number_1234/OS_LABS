#! /bin/bash

if [[ $# -lt 3 ]]
then
    echo "not enough arguments!"
    exit
fi

MAX_NUM=$1

if [[ $2 -gt $MAX_NUM ]]
    then MAX_NUM=$2
fi

if [[ $3 -gt $MAX_NUM ]]
    then MAX_NUM=$3
fi

echo "Max number is $MAX_NUM"
