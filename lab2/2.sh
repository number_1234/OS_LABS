#! /bin/bash

FILE="$PWD/full.log"

while read f; do
    # extract markers like "(II)", "(WW)", etc:
    marker="$(echo ${f} | cut -d " " -f 3)"
    
    if [ ${#marker} -eq 4 ]; then
        line="$(echo ${f} | awk '{for (i=3; i<NF; i++) printf $i " "; print $NF}')"
        case $marker in
            "(II)")
                echo "${line/$marker/'Information:'}" >> $FILE
                ;;
            "(WW)")
                echo "${line/$marker/'Warning:'}" >> $FILE
                ;;
            *)
                : # do nothing
                ;;
        esac
    fi
done </var/log/Xorg.0.log

sort -o $FILE{,}
