#! /bin/bash

FILE="$PWD/man_bash.txt"
man bash > $FILE

declare -A WORDS # create associative array
while read f; do
    line="$(echo $f | tr ' ' '\n')" # split current line by space

    for word in $line; do
      if [ ${#word} -ge 4 ]; then
        if [ -n "${WORDS[$word]}" ];
        then ((WORDS[$word]++))
        else WORDS+=(["${word}"]=1)
        fi
      fi
    done
done <$FILE

echo "amount - word"
for key in ${!WORDS[*]}; do
  echo "${WORDS[${key}]} - ${key}"
done | sort -rn | head -n 3

unset $WORDS
rm $FILE
