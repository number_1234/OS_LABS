#! /bin/bash

while read t; do
    echo "${t}" | awk '{split($0, line, ":"); print line[3], line[1];}'
done </etc/passwd | sort -n
