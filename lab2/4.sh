#! /bin/bash

FILES_LIST="$PWD/files_list.txt"
find /bin/ -type f -exec echo "{}" \; > $FILES_LIST

while read filename; do
    first_line="$(head -n 1 $filename)"
    # first_line="$(sed -n '1p' $t)"
    if [[ "$first_line" == "#!"* ]]; then
        echo "${filename} : ${first_line}"
    fi
done <$FILES_LIST

rm $FILES_LIST

# ./4.sh 2> /dev/null  4.36s user 1.88s system 101% cpu 6.143 total # with head
# ./4.sh 2> /dev/null  8.86s user 4.04s system 88% cpu 14.656 total # with sed