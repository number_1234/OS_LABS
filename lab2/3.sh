#! /bin/bash

grep -rEoh "\b[[:alnum:]._%+-]+@[[:alnum:].-]+\.[[:alpha:]]{2,6}\b" /etc/* 2> /dev/null \
  | uniq \
  | tr -s '\n' ',' > emails.lst
# grep:
# -r - чтоб искал рекурсивно
# -E - extended regular expressions
# -o - только сам шаблон, а не всю строку с ним
# -h - не писать имя файла, где нашёл шаблон