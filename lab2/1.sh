#! /bin/bash

FILE="$PWD/errors.log"
grep -r "ACPI" /var/log 2> /dev/null \
  | awk '{split($0, line, ":"); print line[2]}' > $FILE
cat $FILE
